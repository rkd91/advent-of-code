#[macro_use] extern crate text_io;
use std::collections::HashMap;

fn part2() {
    let mut v: Vec<i32> = Vec::new();
    loop {
        let input: Result<i32, _> = try_read!();
        match input {
            Ok(i) => { v.push(i); },
            Err(e) => { println!("{}", e); break; }
        }
    }

    let mut result: i32 = 0;
    let mut already_seen = HashMap::new();
    for x in v.iter().cycle() {
        result += x;
        println!("Hello, world! {}", result);
        if already_seen.contains_key(&result) {
            break;
        }
        already_seen.insert(result, true);
    }
    println!("Final result: {}", result);
}


fn part1() {
    let mut result: i32 = 0;
    loop {
        let input: Result<i32, _> = try_read!();
        match input {
            Ok(i) => { result += i; },
            Err(e) => { println!("{}", e); break; }
        }
    }
    println!("Hello, world! {}", result);
}

fn main() {
    part2()
}
