#[macro_use] extern crate text_io;

fn runlengths(v : Vec<char>) -> (bool, bool) {
    let mut runoftwo = false;
    let mut runofthree = false;

    let mut lastseen: char = '@'; //v.last().unwrap().to_owned();
    let mut runningcount = 1;
    for c in v {
        if c == lastseen {
            runningcount += 1;
        } else {
            match runningcount {
                2 => { runoftwo = true; }
                3 => { runofthree = true; }
                _ => {}
            };
            lastseen = c;
            runningcount = 1;
        }
    }
    match runningcount {
        2 => { runoftwo = true; }
        3 => { runofthree = true; }
        _ => {}
    };
    (runoftwo, runofthree)
}

fn part1() {
    let mut twocount = 0;
    let mut threecount = 0;
    loop {
        let input: Result<String, _> = try_read!();
        match input {
            Ok(i) => {
                if i == "" { break; };
                let mut characters : Vec<char> = i.chars().collect();
                characters.sort();
                let (runoftwo, runofthree) = runlengths(characters);
                println!("{} {} {}", i, runoftwo, runofthree);
                if runoftwo { twocount += 1; };
                if runofthree { threecount += 1; };
            },
            Err(e) => { println!("{}", e); break; }
        }
    }

    println!("Hello, world! {} {} {}", twocount, threecount, twocount * threecount);
}

fn compare_strings(a: &str, b:  &str) -> bool {
    if a.len() != b.len() {
        return false;
    }

    let mut num_differences = 0;
    let a_chars : Vec<char> = a.chars().collect();
    let b_chars : Vec<char> = b.chars().collect();
    for ii in 0..a.len() {
        if a_chars[ii] != b_chars[ii] {
            num_differences += 1;
        }
    }

    return (num_differences == 1);
}

fn part2() {
    let mut input_strings = Vec::new();
    loop {
        let input: String = read!();
        if input == "" {
            break;
        }
        input_strings.push(input);
    }
    input_strings.sort();

    loop {
        if input_strings.is_empty() { break; }

        let current = input_strings.pop().unwrap();

        for other in &input_strings {
            if compare_strings(&current, &other) {
                println!("{} {}", current, other);
                return;
            }
        }
    }
}

fn main() {
    part2()
}
