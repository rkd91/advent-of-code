extern crate regex;
use regex::Regex;
use std::collections::HashSet;
use std::io::{self, BufRead};

#[derive(Debug, Eq, PartialEq)]
struct FabricInfo {
    id: u32,
    left_offset: usize,
    top_offset: usize,
    width: usize,
    height: usize,
}

fn parse_line(s: &str) -> FabricInfo {
    let re = Regex::new(r"^#(\d+) @ (\d+),(\d+): (\d+)x(\d+)$").unwrap();
    let cg = re.captures(s).unwrap();
    FabricInfo{id: cg.get(1).unwrap().as_str().parse().unwrap(),
               left_offset: cg.get(2).unwrap().as_str().parse().unwrap(),
               top_offset: cg.get(3).unwrap().as_str().parse().unwrap(),
               width: cg.get(4).unwrap().as_str().parse().unwrap(),
               height: cg.get(5).unwrap().as_str().parse().unwrap()}
}

fn cells(f: &FabricInfo, grid_width: usize) -> Vec<usize> {
    let mut ret = Vec::new();
    for x in f.left_offset..f.left_offset+f.width {
        for y in f.top_offset..f.top_offset+f.height {
            let c = (y * grid_width) + x;
            ret.push(c);
        }
    }
    ret.sort();
    assert!(ret.len() == (f.height * f.width));
    ret
}


fn main() {
    let stdin = io::stdin();
    let mut grid_cells : Vec<u32> = Vec::new();
    grid_cells.resize(1000*1000, 0);
    let mut duplicate_cells = HashSet::new();
    let mut all_claims = HashSet::new();
    let mut invalid_claims = HashSet::new();

    for line in stdin.lock().lines() {
        let i = line.unwrap();
        println!("{}", i);
        let fi = parse_line(&i);
        all_claims.insert(fi.id);
        for x in cells(&fi, 1000) {
            if grid_cells[x] == 0 {
                grid_cells[x] = fi.id;
            } else {
                duplicate_cells.insert(x);
                invalid_claims.insert(fi.id);
                invalid_claims.insert(grid_cells[x]);
            }
        }
    }

    println!("{}", duplicate_cells.len());
    println!("{:?}", all_claims.difference(&invalid_claims));
}



#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        let fi = crate::FabricInfo{id: 123, left_offset: 3, top_offset: 2, width: 5, height: 4};
        assert_eq!(crate::parse_line("#123 @ 3,2: 5x4"), fi);
    }

    #[test]
    fn cells() {
        let fi = crate::FabricInfo{id: 123, left_offset: 3, top_offset: 2, width: 5, height: 4};
        assert_eq!(crate::cells(fi, 11), vec![25, 26, 27, 28, 29, 36, 37, 38, 39, 40, 47, 48, 49, 50, 51, 58, 59, 60, 61, 62]);
    }
}
